#include<iostream>
#include<list>
#include<iterator>
using namespace std;

void showtable(list<int> list);		

int main() 
{
	int N, M, i, round = 1;
	
	list <int> table;
	list <int>::iterator potato;
	list <int>::iterator del_person;

	cout << "==========================================================" << endl;
	cout << "              Wellcome to Hot potato game"       << endl;
	cout << "==========================================================" << endl;

	cout << "Enter the number of N : ";
	cin >> N;

	cout << "Enter the number of M : ";
	cin >> M;

	cout << "==========================================================" << endl;

	for (i = 0; i < N; i++)
	{
		table.push_back(i + 1);
	}

	cout << "                     ROUND START!" << endl; 
	cout << "   The table : " ;
	showtable(table);	
	cout << endl;

	cout << "==========================================================" << endl;

	potato = table.begin();

	while (table.size() != 1)
	{
		cout << endl;
		cout << "------------------------- Round " << round << " -------------------------" << endl;
		for (i = 0; i < M; i++)
		{
			cout << "turn " << i + 1 << " -> ";
			potato++;		

			if (potato == table.end())
			{
				potato = table.begin();
			}

			showtable(table);	
			cout << " |  Now person " << *potato << " is holding the potato " << endl;
		}
		del_person = potato;
		
		potato++;		

		if (potato == table.end())
		{
			potato = table.begin();
		}

		cout << "person " << *del_person << " was eliminated !" << endl;
		table.remove(*del_person);		
		cout << "The table : ";
		showtable(table);		
		cout << endl << endl;
		round++;
	}
	cout << "==========================================================" << endl;
	cout << "                  THE WINNER IS PERSON ";
	showtable(table);		
	cout << "! " << endl;
	cout << "==========================================================" << endl;
	return 0;
}

void showtable(list<int> l)
{
	list<int>::iterator it;
	for (it = l.begin(); it != l.end(); it++) 
	{
		cout << *it << " ";
	}

}
